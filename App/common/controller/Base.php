<?php
/**
 * Created by PhpStorm.
 * User: 运营部
 * Date: 2017/8/3
 * Time: 8:56
 */
namespace app\common\controller;
use think\Controller;

class Base extends Controller
{
	protected $login=[];
	public function __construct() {
		parent::__construct();
		$this->checkLogin();
	}
	protected function checkLogin() {
		$functions = get_class_methods($this);
		if(!empty($this->login)) {

			if(in_array('*',$this->login)) {
				self::checK();
			} else {

				foreach ($functions as $key => $value) {
					if(in_array($value,$this->login)) {
						self::check();
					}
				}
			}
		}
	}

	protected function check() {
		if(empty(session('home'))) {
			$this->redirect('Login/login');
		}
	}
}