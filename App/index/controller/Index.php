<?php
/**
 * Created by PhpStorm.
 * User: 运营部
 * Date: 2017/8/3
 * Time: 8:53
 */
namespace app\index\controller;

use app\common\controller\Base;

class Index extends Base
{
    public function index() {
       return  $this->fetch();
    }
}